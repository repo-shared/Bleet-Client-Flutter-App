import 'dart:convert';
import 'package:appfleet/models/user.model.dart';
import 'package:appfleet/services/config.service.dart';
import 'package:http/http.dart' as http;

class ClientService {
  static Future<bool> addClient(UserModel user) async {
    final response = await http
        .post(ConfigService.clientAdd,
            headers: <String, String>{
              'Content-Type': 'application/json; charset=UTF-8',
            },
            body: jsonEncode(<String, String>{
              'id': user.id,
              'DisplayName': user.displayName,
              'Email': user.email,
              'PhotoUrl': user.photoURL,
              'Phone': user.phone,
              'Address': user.address,
              'Latitude': user.latitude,
              'Longitude': user.longitude
            }))
        .timeout(const Duration(seconds: 15));

    if (response.statusCode == 200) {
      return true;
    } else {
      return false;
    }
  }
}
