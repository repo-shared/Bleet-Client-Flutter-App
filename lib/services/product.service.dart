import 'dart:convert';
import 'package:appfleet/models/product.model.dart';
import 'package:appfleet/services/config.service.dart';
import 'package:http/http.dart' as http;

class ProductService {
  static Future<List<ProductModel>> getProducts() async {
    final response = await http
        .get(ConfigService.productGetAll)
        .timeout(const Duration(seconds: 15));

    final parsed = json.decode(response.body).cast<Map<String, dynamic>>();

    var data = parsed
        .map<ProductModel>((json) => ProductModel.fromJson(json))
        .toList();

    return data;
  }
}
