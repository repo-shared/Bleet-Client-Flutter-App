class ConfigService {
  static final String url = 'https://bleet.azurewebsites.net/api/';

  //Product Api
  static final String productGetAll = url + 'product/AllProducts';

  //Client Api
  static final String clientGetById = url + 'client/GetById';
  static final String clientAdd = url + 'client/Add';

  //Order Api
  static final String orderAdd = url + 'order/Add';
}
