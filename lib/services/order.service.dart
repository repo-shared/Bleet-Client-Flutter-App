import 'dart:convert';
import 'package:appfleet/models/order.model.dart';
import 'package:appfleet/services/config.service.dart';
import 'package:http/http.dart' as http;

class OrderService {
  static Future<bool> addOrder(OrderModel order) async {
    final products = jsonDecode(jsonEncode(order.products));
    final objBody = jsonEncode({
      'id': order.id,
      'Date': order.date.toString(),
      'Client': {
        'id': order.client.id,
        'DisplayName': order.client.displayName,
        'Email': order.client.email,
        'PhotoUrl': order.client.photoURL,
        'Phone': order.client.phone,
        'Address': order.client.address,
        'Latitude': order.client.latitude,
        'Longitude': order.client.longitude
      },
      'Products': products,
      'Status': order.status
    });

    final response = await http
        .post(ConfigService.orderAdd,
            headers: <String, String>{
              'Content-Type': 'application/json; charset=UTF-8',
            },
            body: objBody)
        .timeout(const Duration(seconds: 15));

    if (response.statusCode == 200) {
      return true;
    } else {
      return false;
    }
  }
}
