import 'package:appfleet/pages/wait.page.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:appfleet/utils/primaryColors.dart';
import 'package:appfleet/models/product.model.dart';
import 'package:provider/provider.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  runApp(
    ChangeNotifierProvider(
      create: (context) => AppState(),
      child: MyApp(),
    ),
  );
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: 'Bleet',
        theme: ThemeData(primarySwatch: primaryColors),
        home: WaitPage());
  }
}

class AppState with ChangeNotifier {
  List<ProductModel> cartElements = [];

  void addCartElement(ProductModel elem) {
    if (cartElements.where((element) => element.id == elem.id).length ==
        0) {
      elem.quantity = 1;
      cartElements.add(elem);
      notifyListeners();
    }
  }

  void delCartElement(String id) {
    ProductModel elem =
        cartElements.where((element) => element.id == id).first;
    if (elem != null) {
      cartElements.remove(elem);
    }
    notifyListeners();
  }

  void delAllCartElement() {
    cartElements.clear();
    notifyListeners();
  }

  void addQuantityToCartElement(String id) {
    ProductModel elem =
        cartElements.where((element) => element.id == id).first;

    if (elem != null) {
      elem.quantity += 1;
    }

    notifyListeners();
  }

  void quitQuantityToCartElement(String id) {
    ProductModel elem =
        cartElements.where((element) => element.id == id).first;

    if (elem != null) {
      if (elem.quantity > 1) elem.quantity -= 1;
    }

    notifyListeners();
  }

  double getSumCartElements() {
    double sum = 0;
    cartElements.forEach((element) {
      sum += element.price * element.quantity;
    });

    return sum;
  }
}
