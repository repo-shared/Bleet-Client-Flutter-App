import 'package:sqflite/sqflite.dart';

class DBContext {
  Future<Database> init() async {
    var databasesPath = await getDatabasesPath();
    String path = databasesPath + '/bleet.db';

// open the database
    Database database = await openDatabase(path, version: 1,
        onCreate: (Database db, int version) async {
// When creating the db, create the table

      await db.execute(
          'CREATE TABLE user (id TEXT PRIMARY KEY, displayName TEXT, email TEXT, photoURL TEXT, phone TEXT, address TEXT, latitude TEXT, longitude TEXT)');
    });

    return database;
  }
}
