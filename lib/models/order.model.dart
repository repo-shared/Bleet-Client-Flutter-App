import 'package:appfleet/models/user.model.dart';
import 'package:appfleet/models/product.model.dart';

class OrderModel {
  String id;
  DateTime date;
  UserModel client;
  List<ProductModel> products;
  int status;

  OrderModel({this.id, this.client, this.products, this.status, this.date});
}
