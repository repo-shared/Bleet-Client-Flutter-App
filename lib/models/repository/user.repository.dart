import 'package:appfleet/models/dbcontext.dart';
import 'package:appfleet/models/user.model.dart';
import 'package:appfleet/pages/profile/profile.main.page.dart';
import 'package:appfleet/utils/dialogs.dart';
import 'package:flutter/material.dart';
import 'package:sqflite/sqflite.dart';
import 'package:appfleet/services/client.Service.dart';

class UserRepository {
  static Future<void> addUser(UserModel user) async {
    DBContext dbContext = new DBContext();
    Database db = await dbContext.init();

    await db.insert(
      'user',
      user.toMap(),
      conflictAlgorithm: ConflictAlgorithm.replace,
    );

    await ClientService.addClient(user);
  }

  static Future<void> delAllUser() async {
    DBContext dbContext = new DBContext();
    Database db = await dbContext.init();

    await db.delete('user');
  }

  static Future<List<UserModel>> getUsers() async {
    // Get a reference to the database.
    DBContext dbContext = new DBContext();
    Database db = await dbContext.init();

    // Query the table for all The Dogs.
    final List<Map<String, dynamic>> maps = await db.query('user');

    // Convert the List<Map<String, dynamic> into a List<Dog>.
    return List.generate(maps.length, (i) {
      return UserModel(
        id: maps[i]['id'],
        displayName: maps[i]['displayName'],
        email: maps[i]['email'],
        photoURL: maps[i]['photoURL'],
        phone: maps[i]['phone'],
        address: maps[i]['address'],
        latitude: maps[i]['latitude'],
        longitude: maps[i]['longitude'],
      );
    });
  }

  static Future<bool> checkPhoneAndAddressRedirect(
      BuildContext context) async {
    var users = await UserRepository.getUsers();
    var user = users.first;

    if (user.phone.trim() == "" || user.address.trim() == "") {
      Navigator.push(context,
          MaterialPageRoute(builder: (BuildContext context) => ProfilePage()));

        Dialogs.show(
            'Mi perfil',
            'Para continuar favor ingrese su Teléfono y dirección para llamarle y enviarle su pedido.',
            context);
      
      return false;
    }

    return true;
  }
}
