class ProductModel {
  final String id;
  final String name;
  final String description;
  final String type;
  final String imagen;
  final double price;
  final bool active;
  int quantity;

  ProductModel(
      {this.id,
      this.name,
      this.description,
      this.type,
      this.imagen,
      this.price,
      this.active,
      this.quantity});

  factory ProductModel.fromJson(Map<String, dynamic> json) {
    return ProductModel(
        id: json['id'],
        name: json['name'],
        description: json['description'],
        type: json['type'],
        imagen: json['imagen'],
        price: json['price'],
        active: json['active'],
        quantity: json['quantity']);
  }

  Map<String, dynamic> toJson() {
    return {
      "id": this.id,
      "name": this.name,
      "description": this.description,
      "type": this.type,
      "imagen": this.imagen,
      "price": this.price,
      "active": this.active,
      "quantity": this.quantity,
    };
  }
}
