class UserModel {
  String id;
  String displayName;
  String email;
  String photoURL;
  String phone;
  String address;
  String latitude;
  String longitude;

  UserModel(
      {this.id,
      this.displayName,
      this.email,
      this.photoURL,
      this.phone,
      this.address,
      this.latitude,
      this.longitude});

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'displayName': displayName,
      'email': email,
      'photoURL': photoURL,
      'phone': phone,
      'address': address,
      'latitude': latitude,
      'longitude': longitude
    };
  }

  factory UserModel.fromJson(Map<String, dynamic> json) {
    return UserModel(
        id: json['id'],
        displayName: json['displayName'],
        email: json['email'],
        photoURL: json['photoUrl'],
        phone: json['phone'],
        address: json['address'],
        latitude: json['latitude'],
        longitude: json['longitude']);
  }
}
