import 'package:flutter/material.dart';

const MaterialColor primaryColors = const MaterialColor(
  0xFF136e80,
  const <int, Color>{
    50: Color.fromRGBO(19, 110, 128, .1),
    100: Color.fromRGBO(19, 110, 128, .2),
    200: Color.fromRGBO(19, 110, 128, .3),
    300: Color.fromRGBO(19, 110, 128, .4),
    400: Color.fromRGBO(19, 110, 128, .5),
    500: Color.fromRGBO(19, 110, 128, .6),
    600: Color.fromRGBO(19, 110, 128, .7),
    700: Color.fromRGBO(19, 110, 128, .8),
    800: Color.fromRGBO(19, 110, 128, .9),
    900: Color.fromRGBO(19, 110, 128, 1),
  },
);
