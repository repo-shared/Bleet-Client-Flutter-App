import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

import 'app.colors.dart';

class Dialogs {
  static void show(String title, String message, BuildContext context) {
    // flutter defined function
    showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          title: new Text(title),
          content: new Text(message),
          actions: <Widget>[
            // usually buttons at the bottom of the dialog
            new FlatButton(
              child: new Text("Cerrar"),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  static Future<void> showConfirmDialog(
      BuildContext context, Function okOnPressed) async {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          title: Row(
            children: [
              Container(
                height: 40,
                child: Image.asset(
                  'assets/logo.png',
                  fit: BoxFit.fill,
                ),
              ),
              SizedBox(width: 10),
              Container(
                  child: new Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  new Text(
                    'Bleet',
                    textAlign: TextAlign.right,
                    style: TextStyle(
                        color: AppColors.primary,
                        fontWeight: FontWeight.bold,
                        fontSize: 20),
                  ),
                  new Text('Productos de Limpieza'),
                ],
              )),
            ],
          ),
          content: Text('Desea realizar el pedido?'),
          actions: <Widget>[
            // usually buttons at the bottom of the dialog
            new FlatButton(
              child: new Text("Ok"),
              onPressed: okOnPressed,
            ),
            new FlatButton(
              child: new Text("Cerrar"),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  static Future<void> showLoadingDialog(
      BuildContext context, GlobalKey key) async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return new WillPopScope(
          onWillPop: () async => false,
          child: SimpleDialog(
            key: key,
            backgroundColor: Colors.transparent,
            children: <Widget>[
              Container(
                width: 250.0,
                height: 250.0,
                decoration: new BoxDecoration(
                  color: Colors.white,
                  shape: BoxShape.circle,
                ),
                child: Stack(
                  children: [
                    Center(
                      child: SizedBox(
                        height: 220.0,
                        width: 220.0,
                        child: CircularProgressIndicator(
                            valueColor:
                                AlwaysStoppedAnimation(AppColors.primaryDark),
                            strokeWidth: 20.0),
                      ),
                    ),
                    Center(
                      child: Image.asset(
                        "assets/logo.png",
                        width: 120,
                      ),
                    )
                  ],
                ),
              )
            ],
          ),
        );
      },
    );
  }

  static Future<void> showAboutUs(BuildContext context, GlobalKey key) async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return new WillPopScope(
          onWillPop: () async => true,
          child: SimpleDialog(
            key: key,
            backgroundColor: Colors.white,
            contentPadding: EdgeInsets.all(20),
            children: <Widget>[
              Row(
                children: [
                  Container(
                    height: 40,
                    child: Image.asset(
                      'assets/logo.png',
                      fit: BoxFit.fill,
                    ),
                  ),
                  SizedBox(width: 10),
                  Container(
                      child: new Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      new Text(
                        'Bleet',
                        textAlign: TextAlign.right,
                        style: TextStyle(
                            color: AppColors.primary,
                            fontWeight: FontWeight.bold,
                            fontSize: 20),
                      ),
                      new Text('Productos de Limpieza'),
                    ],
                  )),
                ],
              ),
              SizedBox(height: 40),
              Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Text(
                      'Somos una empresa que se enfoca en elaborar y distribuir productos de limpieza.'),
                  SizedBox(height: 20),
                  Row(
                    children: [
                      Container(
                          child: new Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          new Text(
                            'Llamanos',
                            textAlign: TextAlign.right,
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          new Text('82626180'),
                        ],
                      )),
                      SizedBox(
                        width: 10,
                      ),
                      ClipOval(
                        child: Material(
                          color: Colors.white, // button color
                          elevation: 10.0,
                          child: InkWell(
                            splashColor: AppColors.primaryDark, // inkwell color
                            child: SizedBox(
                                width: 40,
                                height: 40,
                                child: Icon(
                                  Icons.phone_in_talk,
                                  color: AppColors.primary,
                                )),
                            onTap: () {
                              launch("tel://+50582626180");
                            },
                          ),
                        ),
                      ),
                    ],
                  ),
                  SizedBox(height: 20),
                  Text('version: 1.0.1'),
                  SizedBox(height: 20),
                  FlatButton(
                    child: Text(
                      "ok",
                      style: TextStyle(color: AppColors.primary, fontSize: 18),
                    ),
                    onPressed: () {
                      Navigator.pop(context);
                    },
                  ),
                ],
              )
            ],
          ),
        );
      },
    );
  }
}
