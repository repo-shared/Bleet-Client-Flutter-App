import 'package:appfleet/utils/app.colors.dart';
import 'package:flutter/material.dart';

final textSizeTitle = 18.0;
final textSizeHead1 = 28.0;
final textSizeSubTitle = 16.0;
final textSizeContent = 16.0;

final kHintTextStyle = TextStyle(
  color: AppColors.editTextLoginColor,
  fontFamily: 'OpenSans',
);

final kLabelStyle = TextStyle(
    color: Colors.white,
    fontWeight: FontWeight.bold,
    fontFamily: 'OpenSans',
    fontSize: textSizeContent);

final kBoxDecorationStyle = BoxDecoration(
  color: AppColors.editTextLoginBackground,
  borderRadius: BorderRadius.circular(10.0),
  boxShadow: [
    BoxShadow(
      color: Colors.black12,
      blurRadius: 6.0,
      offset: Offset(0, 2),
    ),
  ],
);
