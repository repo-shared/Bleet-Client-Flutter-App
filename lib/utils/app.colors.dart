import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class AppColors {
  static final Color primary = Color.fromRGBO(19, 110, 128, 1);
  static final Color primaryDark = Color.fromRGBO(14, 15, 121, 1);

  //Login Colors
  static final Color editTextLoginBackground = Color(0xBBFFFFFF);
  static final Color editTextLoginColor = Color(0xFF7d7b7a);

  //Text Colors
  static final Color textContent = Color(0xFF777777);
}
