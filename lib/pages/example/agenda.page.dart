import 'package:flutter/material.dart';

class AgendaPage extends StatefulWidget {
  AgendaPage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _AgendaPageState createState() => new _AgendaPageState();
}

class _AgendaPageState extends State<AgendaPage> {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        appBar: new AppBar(
          // Here we take the value from the MyHomePage object that was created by
          // the App.build method, and use it to set our appbar title.
          title: new Text(widget.title),
          leading: Builder(
            builder: (context) => IconButton(
                icon: new Icon(Icons.arrow_back),
                onPressed: () => Navigator.pop(context)),
          ),
        ),
        body: Center(child: Text("En espera de listado de citas de agenda")));
  }
}
