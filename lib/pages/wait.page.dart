import 'package:appfleet/pages/home/home.page.dart';
import 'package:appfleet/pages/login/login.page.dart';
import 'package:flutter/material.dart';
import 'package:appfleet/models/repository/user.repository.dart';

import '../models/user.model.dart';

class WaitPage extends StatefulWidget {
  WaitPage({Key key}) : super(key: key);

  @override
  _WaitPageState createState() => new _WaitPageState();
}

class _WaitPageState extends State<WaitPage> {
  @override
  void initState() {
    getCurrentUser();
  }

  getCurrentUser() async {
    UserModel userModel = new UserModel();
    var users = await UserRepository.getUsers();

    if (users.length > 0) {
      Navigator.of(context).pushAndRemoveUntil(
          MaterialPageRoute(builder: (context) => HomePage(title: 'Bleet')),
          (Route<dynamic> route) => false);
    } else {
      Navigator.of(context).pushAndRemoveUntil(
          MaterialPageRoute(builder: (context) => LoginPage(title: 'Bleet')),
          (Route<dynamic> route) => false);
    }
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        body: Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        CircularProgressIndicator(),
        Container(
          height: 20,
        ),
        Text("Espere un momento por favor...")
      ],
    ));
  }
}
