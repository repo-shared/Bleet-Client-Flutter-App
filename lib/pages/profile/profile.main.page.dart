import 'dart:async';

import 'package:appfleet/models/user.model.dart';
import 'package:appfleet/models/repository/user.repository.dart';

import 'package:appfleet/pages/login/login.page.dart';
import 'package:appfleet/utils/app.colors.dart';
import 'package:appfleet/utils/contants.dart';
import 'package:appfleet/utils/dialogs.dart';
import 'package:flutter/material.dart';
import 'package:location/location.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

class ProfilePage extends StatefulWidget {
  ProfilePage({Key key}) : super(key: key);

  @override
  _ProfilePageState createState() => new _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> {
  final TextEditingController phoneController = new TextEditingController();
  final TextEditingController addressController = new TextEditingController();

  LatLng location = LatLng(12.1271764, -86.267002);
  //final Geolocator geolocator = Geolocator()..forceAndroidLocationManager;
  UserModel user = UserModel(
      photoURL: 'https://www.drupal.org/files/issues/default-avatar.png',
      displayName: '',
      email: '',
      phone: '',
      address: '');

  BitmapDescriptor pinLocationIcon;
  Set<Marker> _markers = {};
  Completer<GoogleMapController> _controller = Completer();

  Location mylocation = new Location();

  bool _serviceEnabled;
  PermissionStatus _permissionGranted;
  LocationData _locationData;

  _getCurrentLocation() async {
    var users = await UserRepository.getUsers();

    _serviceEnabled = await mylocation.serviceEnabled();
    if (!_serviceEnabled) {
      _serviceEnabled = await mylocation.requestService();
      if (!_serviceEnabled) {
        return;
      }
    }

    _permissionGranted = await mylocation.hasPermission();
    if (_permissionGranted == PermissionStatus.denied) {
      _permissionGranted = await mylocation.requestPermission();
      if (_permissionGranted != PermissionStatus.granted) {
        return;
      }
    }

    _locationData = await mylocation.getLocation();

    final GoogleMapController controller = await _controller.future;
    final CameraPosition _kLake = CameraPosition(
        target: LatLng(_locationData.latitude, _locationData.longitude),
        zoom: 13,
        bearing: 360,
        tilt: 30);

    controller.animateCamera(CameraUpdate.newCameraPosition(_kLake));
    setState(() {
      user = users.first;

      location = LatLng(_locationData.latitude, _locationData.longitude);

      _markers.clear();
      _markers.add(Marker(
          draggable: true,
          markerId: MarkerId("1"),
          position: location,
          icon: pinLocationIcon,
          onDragEnd: ((newPosition) {
            print(newPosition.latitude);
            print(newPosition.longitude);
          })));
    });
  }

  @override
  void initState() {
    BitmapDescriptor.fromAssetImage(
            ImageConfiguration(devicePixelRatio: 2.5), 'assets/pin.png')
        .then((onValue) {
      pinLocationIcon = onValue;
    });

    _getCurrentLocation();
  }

  final GlobalKey<State> _keyLoader = new GlobalKey<State>();

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        appBar: new AppBar(
          // Here we take the value from the MyHomePage object that was created by
          // the App.build method, and use it to set our appbar title.
          title: new Text('Mi perfil'),
          leading: Builder(
            builder: (context) => IconButton(
                icon: new Icon(Icons.arrow_back),
                onPressed: () => Navigator.pop(context)),
          ),
        ),
        floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
        floatingActionButton: FloatingActionButton.extended(
            backgroundColor: AppColors.primary,
            icon: Icon(Icons.save),
            onPressed: () async {
              user.latitude = location.latitude.toString();
              user.longitude = location.longitude.toString();
              user.phone = phoneController.text;
              user.address = addressController.text;

              Dialogs.showLoadingDialog(context, _keyLoader);

              Timer(Duration(milliseconds: 100), () async {
                // 5s over, navigate to a new page
                await UserRepository.addUser(user);
                Navigator.pop(context);
                Navigator.pop(context);
              });
            },
            label: Text('Guardar')),
        body: Container(
            padding: EdgeInsets.all(20),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(
                            width: 80.0,
                            height: 80.0,
                            decoration: new BoxDecoration(
                                shape: BoxShape.circle,
                                border:
                                    Border.all(color: Colors.white, width: 3),
                                image: new DecorationImage(
                                    fit: BoxFit.fill,
                                    image: new NetworkImage(user.photoURL)))),
                        Padding(
                          padding: EdgeInsets.fromLTRB(0, 10, 0, 0),
                          child: Text(
                            user.displayName,
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                        ),
                        Padding(
                            padding: EdgeInsets.fromLTRB(0, 5, 0, 20),
                            child: Text(
                              user.email,
                              style: TextStyle(),
                            )),
                      ],
                    ),
                    FlatButton(
                      shape: CircleBorder(),
                      color: Colors.transparent,
                      padding: EdgeInsets.all(10.0),
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          Padding(
                            padding: const EdgeInsets.all(4.0),
                            child: Icon(
                              Icons.logout,
                              color: AppColors.primaryDark,
                            ),
                          ),
                          Padding(
                              padding: const EdgeInsets.all(2.0),
                              child: Text(
                                "Desconectar",
                                style: TextStyle(color: AppColors.primaryDark),
                              )),
                        ],
                      ),
                      onPressed: () {
                        UserModel userModel = new UserModel();

                        UserRepository.delAllUser();

                        Navigator.of(context).pop();
                        Navigator.of(context).pushAndRemoveUntil(
                            MaterialPageRoute(
                                builder: (context) =>
                                    LoginPage(title: 'Bleet')),
                            (Route<dynamic> route) => false);
                      },
                    ),
                  ],
                ),
                Container(
                  alignment: Alignment.centerLeft,
                  decoration: kBoxDecorationStyle,
                  height: 50.0,
                  child: TextFormField(
                    controller: phoneController..text = user.phone,
                    keyboardType: TextInputType.phone,
                    style: TextStyle(
                        color: AppColors.editTextLoginColor,
                        fontFamily: 'OpenSans',
                        fontSize: textSizeContent),
                    decoration: InputDecoration(
                      border: InputBorder.none,
                      contentPadding: EdgeInsets.only(top: 14.0),
                      prefixIcon: Icon(
                        Icons.phone,
                        color: AppColors.editTextLoginColor,
                      ),
                      hintStyle: kHintTextStyle,
                      hintText: 'Ingrese su número de Teléfono',
                    ),
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                Container(
                  alignment: Alignment.centerLeft,
                  decoration: kBoxDecorationStyle,
                  height: 50.0,
                  child: TextFormField(
                    controller: addressController..text = user.address,
                    keyboardType: TextInputType.multiline,
                    minLines: 1,
                    maxLines: 3,
                    style: TextStyle(
                        color: AppColors.editTextLoginColor,
                        fontFamily: 'OpenSans',
                        fontSize: textSizeContent),
                    decoration: InputDecoration(
                      border: InputBorder.none,
                      contentPadding: EdgeInsets.only(top: 14.0),
                      prefixIcon: Icon(
                        Icons.pin_drop_sharp,
                        color: AppColors.editTextLoginColor,
                      ),
                      hintStyle: kHintTextStyle,
                      hintText: 'Ingrese su dirección',
                    ),
                  ),
                ),
                Padding(
                    padding: EdgeInsets.fromLTRB(0, 20, 0, 5),
                    child: Text(
                      "Seleccione su ubicación",
                      style: TextStyle(),
                    )),
                Expanded(
                    child: Container(
                        height: double.infinity,
                        decoration: BoxDecoration(
                            borderRadius:
                                BorderRadius.all(Radius.circular(10))),
                        child: GoogleMap(
                          mapType: MapType.normal,
                          zoomControlsEnabled: false,
                          initialCameraPosition: CameraPosition(
                              target: location,
                              zoom: 13,
                              bearing: 360,
                              tilt: 30),
                          markers: _markers,
                          onMapCreated: (GoogleMapController controller) {
                            _controller.complete(controller);

                            setState(() {
                              _markers.add(Marker(
                                  draggable: true,
                                  markerId: MarkerId("1"),
                                  position: location,
                                  icon: pinLocationIcon,
                                  onDragEnd: ((newPosition) {
                                    setState(() {
                                      location = LatLng(newPosition.latitude,
                                          newPosition.longitude);
                                    });
                                  })));
                            });
                          },
                        ))),
              ],
            )));
  }
}
