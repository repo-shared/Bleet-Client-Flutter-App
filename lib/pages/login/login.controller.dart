import 'package:appfleet/pages/home/home.page.dart';
import 'package:flutter/material.dart';

class LoginController {
  static void signIn(String user, String pass, BuildContext context) {
    //Dialogs.show(user, pass, context);
    //LoginService().login();

    Navigator.of(context).pushAndRemoveUntil(
        MaterialPageRoute(builder: (context) => HomePage(title: 'Bleet')),
        (Route<dynamic> route) => false);
  }
}
