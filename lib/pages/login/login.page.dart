import 'dart:async';

import 'package:appfleet/models/user.model.dart';
import 'package:appfleet/models/repository/user.repository.dart';
import 'package:appfleet/pages/home/home.page.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:appfleet/utils/contants.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter_facebook_auth/flutter_facebook_auth.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'login.controller.dart';
import '../../utils/dialogs.dart';

class LoginPage extends StatefulWidget {
  LoginPage({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final TextEditingController emailController = new TextEditingController();
  final TextEditingController passController = new TextEditingController();

  final FirebaseAuth _auth = FirebaseAuth.instance;

  GoogleSignIn _googleSignIn = GoogleSignIn(
    scopes: <String>['email'],
  );

  Future<UserModel> signInWithGoogle() async {
    final GoogleSignInAccount googleSignInAccount =
        await _googleSignIn.signIn().catchError((onError) {
      print("Error $onError");
      
    });

    if (googleSignInAccount == null) {
      Navigator.of(context).pop();
      return null;
    }
    final GoogleSignInAuthentication googleSignInAuthentication =
        await googleSignInAccount.authentication;

    final AuthCredential credential = GoogleAuthProvider.credential(
      accessToken: googleSignInAuthentication.accessToken,
      idToken: googleSignInAuthentication.idToken,
    );

    var authResult = await _auth.signInWithCredential(credential);
    final User user = authResult.user;

    assert(!user.isAnonymous);
    assert(await user.getIdToken() != null);

    final User currentUser = _auth.currentUser;
    assert(user.uid == currentUser.uid);

    UserModel userModel = new UserModel(
        id: user.uid,
        displayName: user.displayName,
        email: user.email,
        photoURL: user.photoURL,
        phone: '',
        address: '',
        latitude: '',
        longitude: '');

    var users = await UserRepository.getUsers();
    if (users.length > 0) {
      userModel = new UserModel(
          id: user.uid,
          displayName: user.displayName,
          email: user.email,
          photoURL: user.photoURL,
          phone: users.first.phone,
          address: users.first.address,
          latitude: users.first.latitude,
          longitude: users.first.longitude);
    }

    return userModel;
  }

  Future<UserModel> signInWithFacebook() async {
    // Trigger the sign-in flow
    final LoginResult result =
        await FacebookAuth.instance.login().catchError((onError) {
      print("Error $onError");
    
    });

    if (result.accessToken == null) {
      Navigator.of(context).pop();
      return null;
    }

    // Create a credential from the access token
    final FacebookAuthCredential facebookAuthCredential =
        FacebookAuthProvider.credential(result.accessToken.token);

    var user = await FirebaseAuth.instance
        .signInWithCredential(facebookAuthCredential);

    UserModel userModel = new UserModel(
        id: user.user.uid,
        displayName: user.user.displayName,
        email: user.user.email,
        photoURL: user.user.photoURL,
        phone: '',
        latitude: '',
        longitude: '');

    return userModel;
  }

  void signOutGoogle() async {
    //await googleSignIn.signOut();
    _googleSignIn.disconnect();
    print("User Sign Out");
  }

  void signOutFacebook() async {
    await FacebookAuth.instance.logOut();
    print("User Sign Out");
  }

/* #region Widgets */
  Widget loginButton() {
    return Container(
      padding: EdgeInsets.symmetric(vertical: 0.0),
      width: double.infinity,
      child: RaisedButton(
        elevation: 5.0,
        onPressed: () {
          Dialogs.showLoadingDialog(context, _keyLoader); //invoking login
          //Navigator.pop(context);

          LoginController.signIn(
              emailController.text, passController.text, context);
        },
        padding: EdgeInsets.all(15.0),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10.0),
        ),
        color: Colors.white,
        child: Text(
          'Iniciar',
          style: TextStyle(
            fontSize: 14.0,
            fontWeight: FontWeight.bold,
            fontFamily: 'OpenSans',
          ),
        ),
      ),
    );
  }

  Widget _buildSocialBtn(Function onTap, AssetImage logo) {
    return GestureDetector(
      onTap: onTap,
      child: Container(
        height: 90.0,
        width: 90.0,
        decoration: BoxDecoration(
          shape: BoxShape.circle,
          color: Colors.white,
          boxShadow: [
            BoxShadow(
              color: Colors.black26,
              offset: Offset(0, 2),
              blurRadius: 6.0,
            ),
          ],
          image: DecorationImage(
            image: logo,
          ),
        ),
      ),
    );
  }

  Widget _buildSocialBtnRow() {
    return Padding(
      padding: EdgeInsets.symmetric(vertical: 0.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: <Widget>[
          _buildSocialBtn(
            () async {
              Dialogs.showLoadingDialog(context, _keyLoader);
              Timer(Duration(milliseconds: 100), () async {
                var user = await signInWithGoogle();

                if (user != null) {
                  signOutGoogle();
                  await UserRepository.addUser(user);

                  Navigator.of(context).pushAndRemoveUntil(
                      MaterialPageRoute(
                          builder: (context) => HomePage(title: 'Bleet')),
                      (Route<dynamic> route) => false);
                }
              });
            },
            AssetImage(
              'assets/ic_login_google.png',
            ),
          ),
          _buildSocialBtn(
            () async {
              Dialogs.showLoadingDialog(context, _keyLoader);
              Timer(Duration(milliseconds: 100), () async {
                var user = await signInWithFacebook();

                if (user != null) {
                  signOutFacebook();
                  await UserRepository.addUser(user);
                  Navigator.of(context).pushAndRemoveUntil(
                      MaterialPageRoute(
                          builder: (context) => HomePage(title: 'Bleet')),
                      (Route<dynamic> route) => false);
                }
              });
            },
            AssetImage(
              'assets/ic_login_facebook.png',
            ),
          ),
        ],
      ),
    );
  }

/* #endregion   */

  final GlobalKey<State> _keyLoader = new GlobalKey<State>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: AnnotatedRegion<SystemUiOverlayStyle>(
        value: SystemUiOverlayStyle.light,
        child: GestureDetector(
          onTap: () => FocusScope.of(context).unfocus(),
          child: Stack(
            children: <Widget>[
              Container(
                height: double.infinity,
                width: double.infinity,
                decoration: BoxDecoration(
                  image: DecorationImage(
                    image: AssetImage("assets/st01fondo1.png"),
                    fit: BoxFit.cover,
                  ),
                ),
              ),
              Container(
                height: double.infinity,
                child: SingleChildScrollView(
                  physics: AlwaysScrollableScrollPhysics(),
                  padding: EdgeInsets.symmetric(
                    horizontal: 60.0,
                    vertical: 60.0,
                  ),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Image.asset('assets/logo.png',
                          width: 150, height: 150, fit: BoxFit.fill),
                      Text(
                        'Bleet',
                        style: TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.bold,
                            fontSize: textSizeHead1),
                      ),
                      SizedBox(
                        height: 5.0,
                      ),
                      Text(
                        'Productos de Limpieza',
                        style: TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.w400,
                            fontSize: textSizeTitle),
                        textAlign: TextAlign.center,
                      ),
                      SizedBox(
                        height: 60.0,
                      ),
                      Text(
                        'Bienvenido',
                        style: TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.bold,
                            fontSize: textSizeHead1),
                      ),
                      SizedBox(
                        height: 10.0,
                      ),
                      Text(
                          'Gracias por instalar la aplicación, para que puedas hacer tus pedidos a los productos puedes iniciar sesion con Google o Facebook',
                          style: TextStyle(
                              color: Colors.white,
                              fontWeight: FontWeight.w400,
                              fontSize: 18),
                          textAlign: TextAlign.center),
                      SizedBox(
                        height: 60.0,
                      ),
                      _buildSocialBtnRow(),
                    ],
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

/*Functions */

}
