import 'package:flutter/cupertino.dart';

class HomeDrawerModel {
  final IconData icon;
  final String title;
  final Widget page;

  HomeDrawerModel(this.icon, this.title, this.page);
}
