import 'package:appfleet/pages/cart/cart.page.dart';
import 'package:appfleet/pages/inventory/inventory.page.dart';
import 'package:appfleet/pages/home/home.widgets/home.drawer.model.dart';
import 'package:appfleet/pages/profile/profile.main.page.dart';
import 'package:appfleet/utils/app.colors.dart';
import 'package:flutter/material.dart';

class HomeDrawerWidget extends StatefulWidget {
  HomeDrawerWidget({Key key}) : super(key: key);

  @override
  _HomeDrawerState createState() => new _HomeDrawerState();
}

class _HomeDrawerState extends State<HomeDrawerWidget> {
  final List<HomeDrawerModel> drawerMenuList = <HomeDrawerModel>[
    new HomeDrawerModel(Icons.person, "Perfil", ProfilePage()),
    new HomeDrawerModel(Icons.archive, "Productos", InventoryPage()),
    new HomeDrawerModel(
        Icons.shopping_basket, "Pedidos", CartPage(title: "Pedidos")),
    //new HomeDrawerModel(Icons.sms, "Atencion al Cliente", AttentionPage()),
    //new HomeDrawerModel(Icons.info, "Sobre Bleet", AboutPage()),
  ];

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: Column(
        children: <Widget>[
          Container(
              padding: new EdgeInsets.fromLTRB(0, 40.0, 0, 10.0),
              decoration: BoxDecoration(
                image: DecorationImage(
                  image: AssetImage("assets/st01fondo1.png"),
                  fit: BoxFit.cover,
                ),
              ),
              width: double.infinity,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  new Container(
                      width: 80.0,
                      height: 80.0,
                      decoration: new BoxDecoration(
                          shape: BoxShape.circle,
                          border: Border.all(color: Colors.white, width: 3),
                          image: new DecorationImage(
                              fit: BoxFit.fill,
                              image: new NetworkImage(
                                  "https://i0.wp.com/www.winhelponline.com/blog/wp-content/uploads/2017/12/user.png?fit=256%2C256&quality=100&ssl=1")))),
                  new Padding(
                    padding: EdgeInsets.fromLTRB(0, 10, 0, 0),
                    child: Text(
                      "Elias Moreno",
                      style: TextStyle(
                          color: Colors.white, fontWeight: FontWeight.bold),
                    ),
                  ),
                  new Padding(
                      padding: EdgeInsets.fromLTRB(0, 5, 0, 0),
                      child: Text(
                        "e.moreno@bleet.com",
                        style: TextStyle(color: Colors.white),
                      ))
                ],
              )),
          // Menu Info Container
          Expanded(
              flex: 1,
              child: Align(
                  alignment: FractionalOffset.bottomCenter,
                  child: Container(
                      child: ListView.builder(
                          itemCount: drawerMenuList.length,
                          itemBuilder: (BuildContext context, int index) {
                            return ListTile(
                              focusColor: AppColors.primary,
                              hoverColor: AppColors.primary,
                              leading: Icon(drawerMenuList[index].icon,
                                  color: AppColors.textContent),
                              title: Text(drawerMenuList[index].title,
                                  style:
                                      TextStyle(color: AppColors.textContent)),
                              trailing: Icon(Icons.arrow_forward,
                                  color: AppColors.textContent),
                              onTap: () {
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (BuildContext context) =>
                                            drawerMenuList[index].page));
                              },
                            );
                          })))),

          Container(
              padding: new EdgeInsets.fromLTRB(0, 10.0, 0, 10.0),
              width: double.infinity,
              child: Column(
                children: <Widget>[
                  Image.asset(
                    "assets/logo.png",
                    height: 100,
                  ),
                  Center(
                    child: Text('Version 1.0.1'),
                  )
                ],
              ))
        ],
      ),
    );
  }
}
