import 'dart:async';

import 'package:appfleet/pages/cart/cart.page.dart';
import 'package:appfleet/pages/home/home.widgets/home.drawer.widget.dart';
import 'package:appfleet/pages/profile/profile.main.page.dart';
import 'package:flutter/material.dart';
import 'package:appfleet/pages/inventory/inventory.page.dart';
import 'package:appfleet/utils/app.colors.dart';
import 'package:appfleet/utils/dialogs.dart';
import 'package:provider/provider.dart';

import '../../main.dart';

class HomePage extends StatefulWidget {
  HomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => new _MyHomePageState();
}

class _MyHomePageState extends State<HomePage> {
  var _scaffoldKey = new GlobalKey<ScaffoldState>();

  HomeDrawerWidget myDrawer = HomeDrawerWidget();

  StatefulWidget nextpage = InventoryPage();
  StatefulWidget selectedpage = InventoryPage();

  bool showPage = true;

  handleTimeout() async {
    setState(() {
      nextpage = selectedpage;
    });

    return new Timer(Duration(milliseconds: 300), navHandleTimeout);
  }

  void navHandleTimeout() {
    setState(() {
      showPage = true;
    });
  }

  setPage(StatefulWidget page) async {
    setState(() {
      showPage = false;
      selectedpage = page;
    });

    return new Timer(Duration(milliseconds: 300), handleTimeout);
  }

  @override
  Widget build(BuildContext context) {
    // This method is rerun every time setState is called, for instance as done
    // by the _incrementCounter method above.
    //
    // The Flutter framework has been optimized to make rerunning build methods
    // fast, so that you can just rebuild anything that needs updating rather
    // than having to individually change instances of widgets.
    final provider = Provider.of<AppState>(context);

    return new Scaffold(
        key: _scaffoldKey,
        drawer: myDrawer,
        appBar: new AppBar(
          backgroundColor: Colors.transparent,
          elevation: 0.0,
          // Here we take the value from the MyHomePage object that was created by
          // the App.build method, and use it to set our appbar title.
          title: new Text(
            widget.title,
            style: TextStyle(color: AppColors.primary),
          ),

          /*leading: Builder(
            builder: (context) => IconButton(
              icon: new Icon(Icons.menu),
              color: AppColors.primary,
              onPressed: () => _scaffoldKey.currentState.openDrawer(),
            ),
          ),*/
          leading: Container(
              padding: EdgeInsets.all(10),
              height: 20,
              width: 20,
              child: Image.asset(
                'assets/logo.png',
                fit: BoxFit.fill,
              )),
        ),
        body: AnimatedOpacity(
          duration: Duration(milliseconds: 300),
          opacity: showPage ? 1.0 : 0.0,
          child: nextpage,
        ),
        bottomNavigationBar: BottomAppBar(
            color: Colors.black,
            shape: AutomaticNotchedShape(
                RoundedRectangleBorder(), StadiumBorder(side: BorderSide())),
            child: Container(
              padding: EdgeInsets.fromLTRB(15, 0, 15, 0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  FlatButton(
                    shape: CircleBorder(),
                    color: Colors.transparent,
                    padding: EdgeInsets.all(10.0),
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.all(4.0),
                          child: Icon(
                            Icons.account_circle,
                            color: Colors.white,
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(2.0),
                          child: Text(
                            "Mi Perfil",
                            style: TextStyle(color: Colors.white),
                          ),
                        ),
                      ],
                    ),
                    onPressed: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (BuildContext context) =>
                                  ProfilePage()));
                    },
                  ),
                  FlatButton(
                    shape: CircleBorder(),
                    color: Colors.transparent,
                    padding: EdgeInsets.all(10.0),
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.all(4.0),
                          child: Icon(
                            Icons.info,
                            color: Colors.white,
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(2.0),
                          child: Text(
                            "Acerca de",
                            style: TextStyle(color: Colors.white),
                          ),
                        ),
                      ],
                    ),
                    onPressed: () {
                      Dialogs.showAboutUs(context, _scaffoldKey);
                    },
                  ),
                ],
              ),
            )),
        floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
        floatingActionButton: new FloatingActionButton.extended(
          backgroundColor: AppColors.primary,
          icon: Icon(Icons.shopping_bag),
          label: Text(
              'Ver Pedido (' + provider.cartElements.length.toString() + ')'),
          onPressed: () {
            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (BuildContext context) => CartPage()));
          },
        ));
  }
}
