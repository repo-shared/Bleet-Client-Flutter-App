import 'dart:async';
import 'package:appfleet/main.dart';
import 'package:appfleet/utils/app.colors.dart';
import 'package:async_loader/async_loader.dart';
import 'package:flutter/material.dart';
import 'package:appfleet/models/product.model.dart';
import 'package:appfleet/services/product.service.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:provider/provider.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';

class InventoryPage extends StatefulWidget {
  InventoryPage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _InventoryPageState createState() => new _InventoryPageState();
}

class _InventoryPageState extends State<InventoryPage> {
  String selectedType = "none";

  List<ProductModel> allData = [];
  List<ProductModel> filterData = [];

  String buscarText = '';
  List<ProductModel> post;
  final GlobalKey<AsyncLoaderState> asyncLoaderState =
      new GlobalKey<AsyncLoaderState>();

  @override
  void initState() {
    super.initState();
  }

  Future<List<ProductModel>> fetchPosts() async {
    var data = await ProductService.getProducts();

    setState(() {
      post = data;
      allData = data;
      filterData = data;
    });

    return data;
  }

  @override
  Widget build(BuildContext context) {
    final provider = Provider.of<AppState>(context);

    var _asyncLoader = new AsyncLoader(
        key: asyncLoaderState,
        initState: () async => await fetchPosts(),
        renderLoad: () => Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                CircularProgressIndicator(),
                Container(
                  height: 20,
                ),
                Text("Obteniendo Productos...")
              ],
            ),
        renderError: ([error]) => Center(
                child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisSize: MainAxisSize.max,
              children: <Widget>[
                Icon(
                  Icons.info,
                  color: Colors.blue,
                  size: 48,
                ),
                Container(
                  height: 20,
                ),
                Text("Verifique su conexión a internet e intentelo nuevamente"),
                Container(
                  height: 20,
                ),
                RaisedButton.icon(
                  icon: Icon(Icons.rotate_left, color: Colors.white),
                  elevation: 5.0,
                  shape: new RoundedRectangleBorder(
                      borderRadius: new BorderRadius.circular(30.0)),
                  color: Colors.teal,
                  label: new Text('Intentar de nuevo',
                      style: new TextStyle(color: Colors.white)),
                  onPressed: () {
                    asyncLoaderState.currentState.reloadState();
                  },
                )
              ],
            )),
        renderSuccess: ({data}) => Container(
              padding: EdgeInsets.fromLTRB(20, 10, 20, 0),
              decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: new BorderRadius.only(
                      topLeft: const Radius.circular(40.0),
                      topRight: const Radius.circular(40.0))),
              child: Column(
                children: <Widget>[
                  Container(
                      padding: EdgeInsets.all(10.0),
                      decoration: BoxDecoration(color: Colors.transparent),
                      width: MediaQuery.of(context).size.width,
                      child: Row(
                        children: <Widget>[
                          Column(
                            children: <Widget>[
                              Icon(
                                Icons.shopping_bag,
                                color: Colors.black,
                              )
                            ],
                          ),
                          Column(
                            children: <Widget>[
                              Text(
                                "  Lista de Productos",
                                style: new TextStyle(color: Colors.black),
                              )
                            ],
                          ),
                          /*Column(
                        children: <Widget>[
                          RaisedButton.icon(
                            icon: Icon(Icons.pregnant_woman),
                            label: Text("Filtrar"),
                            onPressed: () async {
                              var items = await post;
                              items
                                  .where((a) => a.quantity > 0)
                                  .toList()
                                  .forEach((e) => print(e.quantity));
                            },
                          )
                        ],
                      )*/
                        ],
                      )),
                  Container(
                      padding: EdgeInsets.all(5),
                      color: Colors.transparent,
                      child: TextField(
                        onChanged: (text) async {
                          await searchData(selectedType, text);
                        },
                        decoration: InputDecoration(
                            filled: true,
                            fillColor: Color.fromRGBO(229, 229, 229, 1),
                            hintText: "Buscar...",
                            prefixIcon: Icon(Icons.search,
                                color: AppColors.editTextLoginColor),
                            contentPadding: EdgeInsets.all(10),
                            labelStyle: new TextStyle(color: Colors.green),
                            enabledBorder: OutlineInputBorder(
                                borderSide: const BorderSide(
                                    color: Color.fromRGBO(229, 229, 229, 1),
                                    width: 0.0)),
                            focusedBorder: OutlineInputBorder(
                                borderSide: const BorderSide(
                                    color: Color.fromRGBO(229, 229, 229, 1),
                                    width: 0.0))),
                      )),
                  Container(
                    margin:
                        EdgeInsets.symmetric(vertical: 10.0, horizontal: 2.0),
                    height: 80.0,
                    child: Row(
                      mainAxisSize: MainAxisSize.max,
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        InkWell(
                          child: Container(
                            child: Column(children: <Widget>[
                              Container(
                                padding: EdgeInsets.all(12),
                                height: 60,
                                width: 60,
                                child: SvgPicture.network(
                                  'https://www.flaticon.com/svg/static/icons/svg/2917/2917020.svg',
                                  fit: BoxFit.fill,
                                  color: selectedType == '350 ml'
                                      ? Colors.white
                                      : AppColors.editTextLoginColor,
                                ),
                                decoration: BoxDecoration(
                                    color: selectedType == '350 ml'
                                        ? AppColors.primary
                                        : Colors.white,
                                    border: new Border.all(
                                        color: selectedType == '350 ml'
                                            ? Colors.transparent
                                            : AppColors.editTextLoginColor),
                                    borderRadius:
                                        new BorderRadius.circular(10)),
                              ),
                              Text(
                                '350 ml',
                                style: TextStyle(
                                    color: AppColors.editTextLoginColor),
                              )
                            ]),
                          ),
                          onTap: () async {
                            await searchData('350 ml', buscarText);
                          },
                        ),
                        SizedBox(width: 20),
                        InkWell(
                          child: Container(
                            child: Column(children: <Widget>[
                              Container(
                                padding: EdgeInsets.all(12),
                                height: 60,
                                width: 60,
                                child: SvgPicture.network(
                                  'https://www.flaticon.com/svg/static/icons/svg/1265/1265098.svg',
                                  fit: BoxFit.fill,
                                  color: selectedType == '1/2 Litro'
                                      ? Colors.white
                                      : AppColors.editTextLoginColor,
                                ),
                                decoration: BoxDecoration(
                                    color: selectedType == '1/2 Litro'
                                        ? AppColors.primary
                                        : Colors.white,
                                    border: new Border.all(
                                        color: selectedType == '1/2 Litro'
                                            ? Colors.transparent
                                            : AppColors.editTextLoginColor),
                                    borderRadius:
                                        new BorderRadius.circular(10)),
                              ),
                              Text('1/2 Litro',
                                  style: TextStyle(
                                      color: AppColors.editTextLoginColor))
                            ]),
                          ),
                          onTap: () {
                            searchData('1/2 Litro', buscarText);
                          },
                        ),
                        SizedBox(width: 20),
                        InkWell(
                            child: Container(
                              child: Column(children: <Widget>[
                                Container(
                                  padding: EdgeInsets.all(12),
                                  height: 60,
                                  width: 60,
                                  child: SvgPicture.network(
                                    'https://www.flaticon.com/svg/static/icons/svg/931/931332.svg',
                                    fit: BoxFit.fill,
                                    color: selectedType == 'Litro'
                                        ? Colors.white
                                        : AppColors.editTextLoginColor,
                                  ),
                                  decoration: BoxDecoration(
                                      color: selectedType == 'Litro'
                                          ? AppColors.primary
                                          : Colors.white,
                                      border: new Border.all(
                                          color: selectedType == 'Litro'
                                              ? Colors.transparent
                                              : AppColors.editTextLoginColor),
                                      borderRadius:
                                          new BorderRadius.circular(10)),
                                ),
                                Text('Litro',
                                    style: TextStyle(
                                        color: AppColors.editTextLoginColor))
                              ]),
                            ),
                            onTap: () {
                              searchData('Litro', buscarText);
                            }),
                        SizedBox(width: 20),
                        InkWell(
                            child: Container(
                              child: Column(children: <Widget>[
                                Container(
                                  padding: EdgeInsets.all(12),
                                  height: 60,
                                  width: 60,
                                  child: SvgPicture.network(
                                    'https://www.flaticon.com/svg/static/icons/svg/2451/2451671.svg',
                                    fit: BoxFit.fill,
                                    color: selectedType == 'Galon'
                                        ? Colors.white
                                        : AppColors.editTextLoginColor,
                                  ),
                                  decoration: BoxDecoration(
                                      color: selectedType == 'Galon'
                                          ? AppColors.primary
                                          : Colors.white,
                                      border: new Border.all(
                                          color: selectedType == 'Galon'
                                              ? Colors.transparent
                                              : AppColors.editTextLoginColor),
                                      borderRadius:
                                          new BorderRadius.circular(10)),
                                ),
                                Text('Galón',
                                    style: TextStyle(
                                        color: AppColors.editTextLoginColor))
                              ]),
                            ),
                            onTap: () {
                              searchData('Galon', buscarText);
                            }),
                      ],
                    ),
                  ), 
                  Expanded(child: 
                  StaggeredGridView.countBuilder(
                    crossAxisCount: 4,
                    itemCount: filterData.length,
                    itemBuilder: (BuildContext context, int index) => Container(
                                child: Card(
                                    semanticContainer: true,
                                    child: Padding(
                                      padding: EdgeInsets.all(10),
                                      child: Column(
                                          mainAxisSize: MainAxisSize.min,
                                          children: <Widget>[
                                            Image.network(
                                                filterData[index].imagen,
                                                height: 120,
                                                fit: BoxFit.fill),
                                            Align(
                                              alignment: Alignment.topLeft,
                                              child: Text(
                                                  filterData[index].name,
                                                  style:
                                                      TextStyle(fontSize: 16),
                                                  textAlign: TextAlign.left),
                                            ),
                                            SizedBox(
                                              height: 5,
                                            ),
                                            Align(
                                                alignment: Alignment.topLeft,
                                                child: Text(
                                                    filterData[index].type,
                                                    textAlign: TextAlign.left)),
                                            SizedBox(
                                              height: 10,
                                            ),
                                            Row(
                                                mainAxisSize: MainAxisSize.max,
                                                mainAxisAlignment:
                                                    MainAxisAlignment
                                                        .spaceBetween,
                                                children: [
                                                  Align(
                                                    alignment:
                                                        Alignment.topLeft,
                                                    child: Text(
                                                        'C\$ ' +
                                                            filterData[index]
                                                                .price
                                                                .toString(),
                                                        style: TextStyle(
                                                            fontSize: 20,
                                                            color: AppColors
                                                                .primary),
                                                        textAlign:
                                                            TextAlign.left),
                                                  ),
                                                  ClipOval(
                                                    child: Material(
                                                      color: AppColors
                                                          .primaryDark, // button color
                                                      child: InkWell(
                                                        splashColor: AppColors
                                                            .primary, // inkwell color
                                                        child: SizedBox(
                                                            width: 40,
                                                            height: 40,
                                                            child: Icon(
                                                              Icons.add,
                                                              color:
                                                                  Colors.white,
                                                            )),
                                                        onTap: () {
                                                          provider
                                                              .addCartElement(
                                                                  filterData[
                                                                      index]);
                                                        },
                                                      ),
                                                    ),
                                                  ),
                                                ])
                                          ]),
                                    ))),
                    
                    staggeredTileBuilder: (int index) =>
                        new StaggeredTile.fit(2),
                    mainAxisSpacing: 3.0,
                      crossAxisSpacing: 4.0
                  ),
                  )],
              ),
            ));

    return new Scaffold(
        body: Center(
      child: _asyncLoader,
    ));
  }

  Future<void> searchData(String type, String buscar) async {
    setState(() {
      if (selectedType == type) {
        selectedType = 'none';
      } else {
        selectedType = type;
      }

      buscarText = buscar;

      if (selectedType == 'none') {
        filterData = allData
            .where((elem) =>
                elem.name.toUpperCase().contains(buscar.toUpperCase()))
            .toList();
      } else {
        filterData = allData
            .where((elem) =>
                elem.type == selectedType &&
                elem.name.toUpperCase().contains(buscar.toUpperCase()))
            .toList();
      }

/*
      if (selectedType == type) {
        filterData = allData.where((elem) => elem.name == buscar).toList();
        //selectedType = 'none';
      } else {
        selectedType = type;
        filterData = allData
            .where((elem) => elem.type == type || elem.name == buscar)
            .toList();
      }
      */
    });

    return null;
  }
}
