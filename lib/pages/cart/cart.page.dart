import 'dart:async';

import 'package:appfleet/main.dart';
import 'package:appfleet/models/order.model.dart';
import 'package:appfleet/models/repository/user.repository.dart';
import 'package:appfleet/models/user.model.dart';
import 'package:appfleet/services/order.service.dart';
import 'package:appfleet/utils/app.colors.dart';
import 'package:appfleet/utils/dialogs.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:intl/intl.dart';

class CartPage extends StatefulWidget {
  CartPage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _CartPageState createState() => new _CartPageState();
}

class _CartPageState extends State<CartPage> {
  UserModel user = UserModel();
  

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final provider = Provider.of<AppState>(context);
    final GlobalKey<State> _keyLoader = new GlobalKey<State>();

    return new Scaffold(
        appBar: new AppBar(
          title: new Text('Mi Pedido'),
          leading: Builder(
            builder: (context) => IconButton(
                icon: new Icon(Icons.arrow_back),
                onPressed: () => Navigator.pop(context)),
          ),
        ),
        bottomNavigationBar: BottomAppBar(
            color: Colors.black,
            child: Container(
              padding: EdgeInsets.all(20),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text(provider.cartElements.length.toString() + ' Productos',
                      style: TextStyle(
                        color: Colors.white,
                      )),
                  Text('C\$ ' + provider.getSumCartElements().toString(),
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: 20,
                          fontWeight: FontWeight.bold))
                ],
              ),
            )),
        floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
        floatingActionButton: new FloatingActionButton.extended(
          backgroundColor: AppColors.primary,
          icon: Icon(Icons.check_circle),
          label: Text('Comprar'),
          onPressed: () async {
            

            if (provider.cartElements.length == 0) {
              Dialogs.show('Mi pedido',
                  'Ingrese al menos un producto al pedido', context);
              return;
            }

            var check =
                await UserRepository.checkPhoneAndAddressRedirect(context);

            if (!check) {
              return;
            }

            Dialogs.showConfirmDialog(context, () async {
              Navigator.of(context).pop();

              Dialogs.showLoadingDialog(context, _keyLoader);

              Timer(Duration(milliseconds: 100), () async {
                var users = await UserRepository.getUsers();
                user = users.first;
            
                // 5s over, navigate to a new page
                DateTime now = DateTime.now();
                String formattedDate = DateFormat('yyyyMMddkkmmss').format(now);

                var order = new OrderModel();
                order.id = user.id + "-" + formattedDate;
                order.date = new DateTime.now();
                order.client = user;
                order.products = provider.cartElements;
                order.status = 1;
                await OrderService.addOrder(order);

                provider.delAllCartElement();

                Navigator.pop(context);
                Navigator.pop(context);

                Dialogs.show(
                    'Mi Pedido',
                    'Pedido Realizado, en un momento un agente de venta se pondra en contacto con usted',
                    context);
              });
            });
          },
        ),
        body: Container(
            padding: EdgeInsets.only(top: 10),
            child: new ListView.separated(
                separatorBuilder: (context, index) => Divider(
                      color: Colors.grey,
                    ),
                itemCount: provider.cartElements.length,
                itemBuilder: (BuildContext ctxt, int index) {
                  return new Row(
                    children: <Widget>[
                      new Expanded(
                        child: Stack(
                          children: <Widget>[
                            Positioned(
                                child: Container(
                              height: 60,
                              decoration: BoxDecoration(
                                  color: Color.fromRGBO(230, 230, 230, 1),
                                  borderRadius: BorderRadius.only(
                                      topRight: Radius.circular(10),
                                      bottomRight: Radius.circular(10))),
                            )),
                            new Center(
                                child: Image.network(
                              provider.cartElements[index].imagen,
                              height: 140,
                              fit: BoxFit.fill,
                            ))
                          ],
                        ),
                      ),
                      SizedBox(width: 10),
                      new Expanded(
                          flex: 4,
                          child: Stack(
                            children: [
                              Column(
                                children: <Widget>[
                                  Align(
                                    alignment: Alignment.topLeft,
                                    child: Text(
                                        provider.cartElements[index].name,
                                        style: TextStyle(fontSize: 16),
                                        textAlign: TextAlign.left),
                                  ),
                                  SizedBox(
                                    height: 3,
                                  ),
                                  Align(
                                    alignment: Alignment.topLeft,
                                    child: Text(
                                        provider.cartElements[index].description
                                            .replaceAll('• ', ''),
                                        style: TextStyle(fontSize: 12),
                                        textAlign: TextAlign.left),
                                  ),
                                  SizedBox(
                                    height: 3,
                                  ),
                                  Align(
                                    alignment: Alignment.topLeft,
                                    child: Text(
                                        provider.cartElements[index].type
                                            .replaceAll('• ', ''),
                                        style: TextStyle(
                                            fontSize: 14,
                                            fontWeight: FontWeight.bold),
                                        textAlign: TextAlign.left),
                                  ),
                                  SizedBox(
                                    height: 10,
                                  ),
                                  Row(
                                    children: [
                                      ClipOval(
                                        child: Material(
                                          color: AppColors
                                              .primaryDark, // button color
                                          child: InkWell(
                                            splashColor: AppColors
                                                .primary, // inkwell color
                                            child: SizedBox(
                                                width: 30,
                                                height: 30,
                                                child: Icon(
                                                  Icons.chevron_left,
                                                  color: Colors.white,
                                                )),
                                            onTap: () {
                                              provider
                                                  .quitQuantityToCartElement(
                                                      provider
                                                          .cartElements[index]
                                                          .id);
                                            },
                                          ),
                                        ),
                                      ),
                                      SizedBox(
                                        width: 5,
                                      ),
                                      Text(
                                          provider.cartElements[index].quantity
                                              .toString(),
                                          style: TextStyle(fontSize: 18)),
                                      SizedBox(
                                        width: 5,
                                      ),
                                      ClipOval(
                                        child: Material(
                                          color: AppColors
                                              .primaryDark, // button color
                                          child: InkWell(
                                            splashColor: AppColors
                                                .primary, // inkwell color
                                            child: SizedBox(
                                                width: 30,
                                                height: 30,
                                                child: Icon(
                                                  Icons.chevron_right,
                                                  color: Colors.white,
                                                )),
                                            onTap: () {
                                              provider.addQuantityToCartElement(
                                                  provider
                                                      .cartElements[index].id);
                                            },
                                          ),
                                        ),
                                      ),
                                      SizedBox(
                                        width: 5,
                                      ),
                                      Text('x'),
                                      SizedBox(
                                        width: 5,
                                      ),
                                      Text(
                                          'C\$ ' +
                                              provider.cartElements[index].price
                                                  .toString(),
                                          style: TextStyle(
                                              fontSize: 20,
                                              color: AppColors.primary),
                                          textAlign: TextAlign.left),
                                      SizedBox(
                                        width: 5,
                                      ),
                                      Icon(Icons.arrow_right),
                                      Text(
                                          (provider.cartElements[index].price *
                                                  provider.cartElements[index]
                                                      .quantity)
                                              .toString(),
                                          style: TextStyle(
                                              fontSize: 20,
                                              fontWeight: FontWeight.bold,
                                              color: AppColors.primary),
                                          textAlign: TextAlign.left),
                                    ],
                                  ),
                                ],
                              ),
                              Positioned(
                                  right: 10,
                                  bottom: 0,
                                  child: ClipOval(
                                    child: Material(
                                      color: Colors.red, // button color
                                      child: InkWell(
                                        splashColor:
                                            AppColors.primary, // inkwell color
                                        child: SizedBox(
                                            width: 40,
                                            height: 40,
                                            child: Icon(
                                              Icons.delete,
                                              color: Colors.white,
                                            )),
                                        onTap: () {
                                          provider.delCartElement(
                                              provider.cartElements[index].id);
                                        },
                                      ),
                                    ),
                                  ))
                            ],
                          )),
                    ],
                  );
                })));
  }
}
